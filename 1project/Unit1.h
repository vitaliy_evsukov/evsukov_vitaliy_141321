//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class TFM : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TToolBar *ToolBar1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TButton *buBack;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TLabel *Label2;
	TTabItem *TabItem3;
	TLabel *Label3;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TTabItem *TabItem4;
	TButton *Button11;
	TLabel *Label4;
	TGridPanelLayout *GridPanelLayout2;
	TButton *Button6;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TTimer *Timer1;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *Label7;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Forma(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
private:	// User declarations
    TDateTime FDateTime;
public:		// User declarations
	__fastcall TFM(TComponent* Owner);
    bool Get_True_Or_False(TObject *Sender);
};
//---------------------------------------------------------------------------
extern PACKAGE TFM *FM;
//---------------------------------------------------------------------------
#endif
